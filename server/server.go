package server

import (
	"log"
	"net/http"
	"os"

	"github.com/julienschmidt/httprouter"
)

// Server is the struct for server
type Server struct {
	database *MockDB
}

// GetDatabase returns the database
func (obj *Server) GetDatabase() *MockDB {
	return obj.database
}

// Run runs the server
func Run() {
	var serverObj Server
	var databaseObj MockDB

	serverObj.database = &databaseObj

	router := httprouter.New()
	serverObj.initRoutes(router)

	endpoint := "localhost:" + os.Getenv("PORT")
	log.Println(endpoint)
	log.Fatal(http.ListenAndServe(endpoint, router))
}
