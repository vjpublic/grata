package server

type MockDB struct {
}

var objs []*MyModel
var idOffset = 1

func (obj *MockDB) insert(modelObj *MyModel) error {
	modelObj.ID = idOffset
	idOffset = idOffset + 1
	objs = append(objs, modelObj)
	return nil
}

func (obj *MockDB) update(modelObj *MyModel, id int) error {
	for _, tmpObj := range objs {
		if tmpObj.ID == id {
			tmpObj.Text = modelObj.Text
		}
	}

	return nil
}

func (obj *MockDB) findAll() ([]*MyModel, error) {
	return objs, nil
}
