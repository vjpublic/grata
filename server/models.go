package server

import (
	"context"
)

// User is struct to describe course objects
type MyModel struct {
	ID   int    `json:"id,omitempty"`
	Text string `json:"text"`
	File []byte `json:"file"`
}

// Validate performs minimal validation
func (obj *MyModel) Validate() error {
	return nil
}

// Save saves the object in database
func (obj *MyModel) Save(ctx context.Context, database *MockDB) (*MyModel, error) {
	var err error

	err = obj.Validate()
	if err != nil {
		return nil, err
	}

	err = database.insert(obj)
	if err != nil {
		return nil, err
	}

	return obj, nil
}

// AddFile adds the file
func (obj *MyModel) Update(ctx context.Context, database *MockDB, id int) (*MyModel, error) {
	var err error

	err = obj.Validate()
	if err != nil {
		return nil, err
	}

	err = database.update(obj, id)
	if err != nil {
		return nil, err
	}

	return obj, nil
}

// FindByEmail by email
func (obj *MyModel) FindAll(ctx context.Context, database *MockDB) ([]*MyModel, error) {
	var err error
	var objs []*MyModel

	objs, err = database.findAll()
	if err != nil {
		return nil, err
	}

	return objs, nil
}
