package server

import "github.com/julienschmidt/httprouter"

func (obj *Server) initRoutes(router *httprouter.Router) {
	router.GET("/", obj.Index)
	router.GET("/all", obj.GetEx)
	router.POST("/", obj.PostEx)
	router.PUT("/:id/:text", obj.PutEx)
}
