package server

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/julienschmidt/httprouter"
)

func (obj *Server) PostEx(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var modalObj MyModel
	var err error

	err = r.ParseForm()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to create object."))
		return
	}

	text := r.PostFormValue("text")
	modalObj.Text = text

	createdModel, err := modalObj.Save(context.TODO(), obj.GetDatabase())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to create object."))
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(createdModel)
}

func (obj *Server) GetEx(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var modalObj MyModel
	var err error

	objs, err := modalObj.FindAll(context.TODO(), obj.GetDatabase())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to find objects."))
	}

	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(objs)
}

func (obj *Server) Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	indexMsg := "HELLO!\n"
	indexMsg = indexMsg + "GET / - this page\n"
	indexMsg = indexMsg + "GET /all - Gets all objects at server\n"
	indexMsg = indexMsg + "POST / - Create object at server with text field\n"
	indexMsg = indexMsg + "PUT / - Uploads file\n"

	fmt.Fprintf(w, indexMsg)
}

// Reference: https://tutorialedge.net/golang/go-file-upload-tutorial/
func (obj *Server) PutEx(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	var modalObj MyModel
	var err error

	id := ps.ByName("id")
	text := ps.ByName("text")

	modalObj.Text = text

	idInt, err := strconv.Atoi(id)
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to update object. Strconv Failed"))
		return
	}

	updatedModel, err := modalObj.Update(context.TODO(), obj.GetDatabase(), idInt)
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Failed to update object. Model Failed"))
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(updatedModel)
}
