package logger

import (
	log "github.com/sirupsen/logrus"
)

// Fatal is wrapper for log Fatal
func Fatal(debugMode int, s ...string) {
	if debugMode == 1 {
		log.Fatal(s)
	}
}

// Println is wrapper for log Println
func Println(debugMode int, s ...string) {
	if debugMode == 1 {
		log.Println(s)
	}
}
